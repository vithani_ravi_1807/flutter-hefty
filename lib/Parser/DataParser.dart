import 'dart:async';
import 'dart:convert';

import 'package:flutter_app2/Constants.dart';
import 'package:flutter_app2/Model/DataModel.dart';
import 'package:http/http.dart';



class Dataparser {

  static Future getCategoryList(String url) async {

    try {
      final response = await get(Uri.encodeFull(url));
      if (response.statusCode == 200) {
        Map body = json.decode(response.body);
        if (body != null) {
          if (body.containsKey("Search")) {
            List categories = body["Search"];
            List<DataModel> categoryList = categories.map((c) => new DataModel.fromMap(c)).toList();
            return Constants.resultInApi(categoryList, false);
          }else{
            return Constants.resultInApi("body doesn't contain code",true);
          }
        }else{
          return Constants.resultInApi("body is null",true);
        }
      }
    } catch (e) {
      return Constants.resultInApi("errorStack = "+e.toString(),true);
    }
  }

}