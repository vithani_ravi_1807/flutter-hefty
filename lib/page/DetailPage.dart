import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app2/Constants.dart';
import 'package:flutter_app2/Model/DataModel.dart';
import 'package:flutter_app2/page/NoInternetScreen.dart';
import 'package:timeago/timeago.dart' as timeago;

class DetailPage extends StatefulWidget {
  final DataModel dataModel;

  DetailPage({this.dataModel});

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  bool isInternetAvailable;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: layoutMain());
  }

  @override
  void initState() {
    checkInternet();
    super.initState();
  }

  Widget layoutMain() {
    if (isInternetAvailable != null && isInternetAvailable) {
      return Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              new CachedNetworkImage(
                imageUrl: widget.dataModel.mPoster,
                height: MediaQuery.of(context).size.height / 1.5,
                width: double.infinity,
                fit: BoxFit.fill,
                placeholder: (context, url) => Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    width: double.infinity,
                    child: Center(child: CircularProgressIndicator())),
                errorWidget: (context, url, error) => Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.grey[100]),
                  child: Image.asset(
                    "image/no_image.png",
                    height: MediaQuery.of(context).size.height / 1.5,
                    width: double.infinity,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 28),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: new IconButton(icon: Icon(Icons.arrow_back,color: Colors.white,), onPressed: (){
                    Navigator.of(context).pop();
                  }),
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            widget.dataModel.mTitle,
            style: Theme.of(context).textTheme.title,
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 20,
          ),
          dateDisplay(),
          SizedBox(
            height: 10,
          ),
          Text(
            widget.dataModel.mType,
            style: Theme.of(context).textTheme.body2,
            textAlign: TextAlign.center,
          ),
        ],
      );
    } else if (isInternetAvailable != null && !isInternetAvailable) {
      return NoInternetScreen(
        onTapRetryButton: () {
          checkInternet();
        },
      );
    }
    return Center(child: CircularProgressIndicator());
  }

  Widget dateDisplay() {
    String releaseYear = "";
    if (widget.dataModel.mYear != null) {
      if (widget.dataModel.mYear.toString().contains("\–")) {
        List releaseYearList = widget.dataModel.mYear.split("\–");
        if (releaseYearList.last != null && releaseYearList.last.isNotEmpty) {
          releaseYear = releaseYearList.last;
        } else if (releaseYearList.first != null &&
            releaseYearList.first.isNotEmpty) {
          releaseYear = releaseYearList.first;
        }
      } else {
        releaseYear = widget.dataModel.mYear;
      }
    }

    if (releaseYear.isEmpty) {
      return Container();
    }

    String time = timeago.format(DateTime(int.parse(releaseYear), 1, 1));

    return Text(
      time,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.body2,
    );
  }

  checkInternet() async {
    isInternetAvailable = await Constants.isInternetAvailable();
    setState(() {});
  }
}
