import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app2/Model/DataModel.dart';
import 'package:flutter_app2/Parser/DataParser.dart';
import 'package:flutter_app2/page/DetailPage.dart';
import 'package:flutter_app2/page/NoInternetScreen.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../Constants.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int page = 0;
  bool isInternetAvailable;
  bool isLoading = false;
  bool isPageLoad = false;

  List<DataModel> categoryList = new List<DataModel>();

  var now = new DateTime.now();

  @override
  void initState() {
    checkInternet();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hefty Innovations"),
      ),
      body: layout(),
      resizeToAvoidBottomPadding: false,
    );
  }

  Widget layout() {
    if (isInternetAvailable != null && isInternetAvailable) {
      if (!isLoading) {
        if (isPageLoad != null && isPageLoad) {
          callApiForData(page.toString());
        }
        return Container(
          child: buildList(),
        );
      }else{
        callApiForData(1);
      }
    } else if (isInternetAvailable != null && !isInternetAvailable) {
      return NoInternetScreen(
        onTapRetryButton: () {
          checkInternet();
        },
      );
    }
    return Center(child: CircularProgressIndicator());
  }

  Widget buildList() {
    return LazyLoadScrollView(
      onEndOfPage: () {
        page = page + 1;
        isPageLoad = true;
        setState(() {});
      },
      scrollOffset: 150,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: isPageLoad ? categoryList.length + 1 : categoryList.length,
        itemBuilder: (BuildContext context, int index) {
          if (isPageLoad == true && index == categoryList.length) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailPage(
                          dataModel: categoryList[index],
                        )),
              );
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 6.0, right: 6.0),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CachedNetworkImage(
                            imageUrl: categoryList[index].mPoster,
                            imageBuilder: (context, imageProvider) => Container(
                              height: 150,
                              width: 100,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => Container(
                                width: 100,
                                height: 150,
                                child:
                                    Center(child: CircularProgressIndicator())),
                            width: 100,
                            height: 150,
                            errorWidget: (context, url, error) => Container(
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  color: Colors.grey[100]),
                              child: Image.asset(
                                "image/no_image.png",
                                height: 150,
                                width: 100,
                              ),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.fromLTRB(6.0, 0.0, 6.0, 0.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  categoryList[index].mTitle,
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context).textTheme.title,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "IMDBID: ",
                                      style: Theme.of(context).textTheme.body2,
                                    ),
                                    Text(
                                      categoryList[index].mImdbID,
                                      style: Theme.of(context).textTheme.body1,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Container(
                        width: double.infinity,
                        height: 1,
                        color: Colors.grey,
                      ),
                      new Row(
                        children: <Widget>[
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: dateDisplay(index, context),
                          )),
                          Container(
                            width: 1,
                            height: 45,
                            color: Colors.grey,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                categoryList[index].mType,
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.body2,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget dateDisplay(int index, BuildContext context) {
    if (categoryList != null && categoryList.length == 0) {
      return Container();
    }
    String releaseYear = "";
    if (categoryList[index].mYear != null) {
      if (categoryList[index].mYear.toString().contains("\–")) {
        List releaseYearList = categoryList[index].mYear.split("\–");
        if (releaseYearList.last != null && releaseYearList.last.isNotEmpty) {
          releaseYear = releaseYearList.last;
        } else if (releaseYearList.first != null &&
            releaseYearList.first.isNotEmpty) {
          releaseYear = releaseYearList.first;
        }
      } else {
        releaseYear = categoryList[index].mYear;
      }
    }

    if (releaseYear.isEmpty) {
      return Container();
    }

    String time = timeago.format(DateTime(int.parse(releaseYear), 1, 1));

    return Text(
      time,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.body2,
    );
  }

  Future callApiForData(page) async {
    try {
      Map result = await Dataparser.getCategoryList(
          "http://www.omdbapi.com/?s=Batman&page=$page&apikey=eeefc96f");
      Constants.progressDialog(true, context);
      if (!result["isError"]) {
        Constants.progressDialog(false, context);
        if (isLoading) {
          categoryList = result["value"];
        } else if (isPageLoad) {
          categoryList.addAll(result["value"]);
        }
      } else {
        String msg = result["value"];
        Constants.progressDialog(false, context);
      }
    } catch (e) {
      print(e);
    }

    isLoading = false;
    isPageLoad = false;
    setState(() {});
  }

  checkInternet() async {
    isInternetAvailable = await Constants.isInternetAvailable();
    isLoading = true;
    setState(() {});
  }
}
