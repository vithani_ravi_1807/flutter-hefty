import 'package:flutter/material.dart';

class NoInternetScreen extends StatefulWidget {
  final VoidCallback onTapRetryButton;

  NoInternetScreen({this.onTapRetryButton});

  @override
  _NoInternetScreenState createState() => _NoInternetScreenState();
}

class _NoInternetScreenState extends State<NoInternetScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "image/no_internet.png",
              width: 150,
              height: 150,
            ),
            Text("No Internet Connection",
                softWrap: false,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 18.0,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w400,
                )),
            SizedBox(height: 15.0),
            Text(
              "Please check your connection status and try again.",
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 20.0),
            RaisedButton(
              elevation: 8.0,
              onPressed: widget.onTapRetryButton,
              child: Text("Retry"),
            ),
          ],
        ),
      ),
    );
  }
}
