class DataModel {
  String mTitle;
  String mYear;

  String mImdbID;
  String mType;
  String mPoster;

  DataModel({this.mTitle, this.mYear, this.mImdbID, this.mType, this.mPoster});

  DataModel.fromMap(Map<String, dynamic> map) {
    mTitle = map['Title'].toString();
    mYear = map['Year'].toString();
    mImdbID = map['imdbID'].toString();

    mType = map['Type'].toString();

    print("your Yours=========>" + mYear.toString());
    mPoster = map['Poster'].toString();

    print("your Image=========>" + mPoster.toString());
  }
}
